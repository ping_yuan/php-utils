<?php

namespace dkou\utils;
/**
 * 参数验证相关函数集
 * Class ValidateUtils
 * @package dkou\utils
 */
class ValidateUtils
{
    /**
     * 是否全中文字符串
     * @param $str
     * @return bool
     */
    public static function isCn($str)
    {
        if (preg_match("/[\x{4e00}-\x{9fa5}]+/u", $str)) {
            return true;
        }
        return false;
    }

    /**
     * 是否全数字
     * @param $number
     * @return bool
     */
    public static function isNumber($number)
    {
        if (preg_match('/^\d+$/', $number)) {
            return true;
        }
        return false;
    }

    /**
     * 是否手机号
     * @param $number
     * @return bool
     */
    public static function isPhone($number)
    {
        if (preg_match('/^((\(d{2,3}\))|(\d{3}\-))?1(3|4|5|6|7|8|9)\d{9}$/', $number)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为电话号码
     * @param $number
     * @return bool
     */
    public static function isTel($number)
    {
        if (preg_match('/^[+]{0,1}[\(]?(\d){1,3}[\)]?[ ]?([-]?((\d)|[ ]){1,12})+$/', $number)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为合法金额
     * @param $amount  
     * @param $length int 整数部分最大长度
     * @return bool
     */
    public static function isMoney($amount, $length = 8)
    {
        if (preg_match('/^[0-9]{1,' . $length . '}[.]{0,1}[0-9]{0,2}$/', $amount)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为合法的日期格式
     * @param $date
     * return bool
     */
    public static function isDate($date)
    {
        if (preg_match('/[\d]{4}-[\d]{1,2}-[\d]{1,2}/', $date)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为合法的日期格式
     * @param $date
     * return bool
     */
    public static function isDatetime($time)
    {
        if (preg_match('/[\d]{4}-[\d]{1,2}-[\d]{1,2}\s[\d]{1,2}:[\d]{1,2}:[\d]{1,2}/', $time)) {
            return true;
        }
        return false;
    }
    
    /**
     * 是否中国身份证（非严格，需要严格可以使用IdCardUtils）
     * @param $number
     * @return bool
     */
    public static function isIdCard($number)
    {
        if (preg_match('/(^([\d]{15}|[\d]{18}|[\d]{17}x)$)/', $number)) {
            return true;
        }
        return false;
    }

    /**
     * 是否银行卡
     * @param $number
     * @return bool
     */
    public static function isBankCard($number)
    {
        if ($number == null || $number == "") {
            return false;
        }
        //取出最后一位
        $last = substr($number, (strlen($number) - 1), 1);
        //前15或18位
        $front_last = substr($number, 0, strlen($number) - 1);
        $front_arr = Array();
        //将前置部分号码存入数组(前15或18位)
        $i = strlen($front_last) - 1;
        for ($i; $i > -1; $i--) {
            //前15或18位倒序存进数组
            array_push($front_arr, substr($front_last, $i, 1));
        }
        $sum1 = $sum2 = $sum3 = 0;
        for ($j = 0; $j < count($front_arr); $j++) {
            if (($j + 1) % 2 == 1) {
                // 奇数数字和
                if (intval($front_arr[$j]) * 2 < 9) {
                    $sum1 += intval($front_arr[$j]) * 2;
                } else {
                    $str = intval($front_arr[$j]) * 2;
                    $str1 = 1;
                    $str2 = $str % 10;
                    $sum2 += $str1;
                    $sum2 += $str2;
                }
            } else {
                // 偶数数字和
                $sum3 += intval($front_arr[$j]);
            }
        }
        $sum = $sum1 + $sum2 + $sum3;
        $luhn = $sum % 10 == 0 ? 0 : 10 - $sum % 10;
        if ($luhn == intval($last)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 是否有效的url地址
     * @param $url
     * @return bool
     */
    public static function isUrl($url)
    {
        if (preg_match('/http[s]?:\/\/[\w.]+[\w\/]*[\w.]*\??[\w=&\+\%]*/is', $url)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为邮箱
     * @param $email
     * @return bool
     */
    public static function isEmail($email)
    {
        return !!filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * 是否为ip地址
     * @param $ip
     * @return bool
     */
    public static function isIp($ip)
    {
        return !!filter_var($ip, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6);
    }

    /**
     * 是否为ipv4地址
     * @param $ip
     * @return bool
     */
    public static function isIpV4($ip)
    {
        return !!filter_var($ip, FILTER_FLAG_IPV4);
    }

    /**
     * 是否为ipv6地址
     * @param $ip
     * @return bool
     */
    public static function isIpV6($ip)
    {
        return !!filter_var($ip, FILTER_FLAG_IPV6);
    }
    
}
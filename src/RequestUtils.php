<?php

namespace dkou\utils;
/**
 * 网络请求相关函数集
 * Class RequestUtils
 * @package dkou\utils
 */
class RequestUtils
{
    /**
     * 获取客户端IP地址
     * @param  mixed   $type 返回类型 0|false 返回IP地址 1|true 返回IPV4地址数字
     * @param  boolean $adv  是否进行高级模式获取（有可能被伪装）---代理情况
     * @return mixed
     */
    public static function getClientIp($type = 0,$adv=false)
    {
        $type =  $type ? 1 : 0;
        static $ip = null;
        if ($ip !== null) return $ip[$type];
        if($adv){
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $pos = array_search('unknown',$arr);
                if(false !== $pos) unset($arr[$pos]);
                $ip = trim($arr[0]);
            }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

    /**
     * 判断是否移动端浏览器
     * @return boolean
     */
    public static function isMobileBrowser()
    {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if(isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }
        // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if(isset($_SERVER['HTTP_VIA']) && stristr($_SERVER['HTTP_VIA'], "wap")) {
            return  true;
        }
        // userAgent匹配
        if (isset ($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array(
                'nokia', 'sony', 'ericsson', 'mot', 'samsung',
                'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips',
                'panasonic', 'alcatel', 'lenovo', 'iphone',
                'ipod', 'blackberry', 'meizu',
                'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm',
                'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp',
                'wap', 'mobile'
            );
            if(preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            {
                return true;
            }
        }
        // 协议法，因为有可能不准确，放到最后判断
        if(isset ($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否微信浏览器内打开
     * @param  string  $userAget 可选参数 用户浏览器useAgent头
     * @return boolean
     */
    public static function isWeChatBrowser($userAget = '')
    {
        if(!$userAget) {
            $userAget = $_SERVER['HTTP_USER_AGENT'];
        }
        if ( strpos($userAget, 'MicroMessenger') !== false ) {
            return true;
        }
        return false;
    }

    /**
     * 获取浏览器UserAgent
     * @return string
     */
    function getUserAgent()
    {
        $user_agent = '';
        $server_user_agent = $_SERVER["HTTP_USER_AGENT"];
        if (empty($server_user_agent)) {
            return '';
        } elseif (strpos($server_user_agent, "MSIE")) {
            if (strpos($server_user_agent, "MSIE 6.0")) {
                $user_agent = 'IE6';
            } elseif (strpos($server_user_agent, "MSIE 7.0")) {
                $user_agent = 'IE7';
            } elseif (strpos($server_user_agent, "MSIE 8.0")) {
                $user_agent = 'IE8';
            } elseif (strpos($server_user_agent, "MSIE 9.0")) {
                $user_agent = 'IE9';
            } elseif (strpos($server_user_agent, "MSIE 10.0")) {
                $user_agent = 'IE10';
            } else {
                $user_agent = 'IE';
            }
        } elseif (strpos($server_user_agent, "Netscape")) {
            $user_agent = 'Netscape';
        } elseif (strpos($server_user_agent, "Firefox")) {
            $user_agent = 'Firefox';
        } elseif (strpos($server_user_agent, "MicroMessenger")) {
            $user_agent = 'Weixin';
        } elseif (strpos($server_user_agent, "Chrome")) {
            if (strpos($server_user_agent, "UCBrowser")) {
                $user_agent = 'UCBrowser';
            } elseif (strpos($server_user_agent, "QQBrowser")) {
                $user_agent = 'QQBrowser';
            } elseif (strpos($server_user_agent, "BIDUBrowser")) {
                $user_agent = 'BIDUBrowser';
            } elseif (strpos($server_user_agent, "LBBROWSER")) {
                $user_agent = 'LBBROWSER';
            } elseif (strpos($server_user_agent, "MetaSr")) {
                $user_agent = 'SOGOU';
            } elseif (strpos($server_user_agent, "Maxthon")) {
                $user_agent = 'Maxthon';
            } elseif (strpos($server_user_agent, "TheWorld")) {
                $user_agent = 'TheWorld';
            } elseif (strpos($server_user_agent, "Edge")) {
                $user_agent = 'IE Edge';
            } else {
                $user_agent = 'Chrome';
            }
            return $user_agent;
        } elseif (strpos($server_user_agent, "Safari")) {
            $user_agent = 'Safari';
        } elseif (strpos($server_user_agent, "Opera")) {
            $user_agent = 'Opera';
        } elseif (strpos($server_user_agent, "UCWEB")) {
            $user_agent = 'UCWEB';
        } else {
            $user_agent = 'other';
        }
        return $user_agent;
    }

    /**
     * 获取客户端操作系统
     * @return string
     */
    function getOS()
    {
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            return 'Unknown';
        }
        $os = '';
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (strpos($agent, 'win') !== false) {
            if (strpos($agent, 'nt 10.0') !== false) {
                $os = 'Windows 10';
            } elseif (strpos($agent, 'nt 6.1') !== false) {
                $os = 'Windows 7';
            } elseif (strpos($agent, 'nt 5.1') !== false) {
                $os = 'Windows XP';
            } elseif (strpos($agent, 'nt 5.2') !== false) {
                $os = 'Windows 2003';
            } elseif (strpos($agent, 'nt 5.0') !== false) {
                $os = 'Windows 2000';
            } elseif (strpos($agent, 'nt 6.0') !== false) {
                $os = 'Windows Vista';
            } elseif (strpos($agent, 'nt') !== false) {
                $os = 'Windows NT';
            }
        } elseif (strpos($agent, 'android') !== false) {
            $os = 'Android';
        } elseif (strpos($agent, 'linux') !== false) {
            $os = 'Linux';
        } elseif (strpos($agent, 'mac') !== false) {
            $os = 'Apple';
        } else {
            $os = 'Unknown';
        }
        return $os;
    }


}
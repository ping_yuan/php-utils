<?php

namespace dkou\utils;
/**
 * 服务器相关函数库
 * Class ServerUtils
 * @package dkou\utils
 */
class ServerUtils
{
    /**
     * 判断是否SSL协议
     * @return boolean
     */
    public static function isSsl()
    {
        if(isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))) {
            return true;
        }elseif(isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'] )) {
            return true;
        }
        return false;
    }
    
    /**
     * 判断是否cli模式运行
     * @return boolean
     */
    public static function isUnderCli() : bool
    {
        return !!preg_match("/cli/i", php_sapi_name());
    }
    
}
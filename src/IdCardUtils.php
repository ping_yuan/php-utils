<?php

namespace dkou\utils;
/**
 * 身份证相关函数集
 * Class IdCardUtils
 * @package dkou\utils
 */
class IdCardUtils
{
    /**
     * 身份证校验
     * @param string $idCard 身份证号码
     * @param bool   $strict 是否严格模式，false->只检查是否符合位数，true->根据国家标准
     * @return bool
     */
    public static function isIdCard($idCard, $strict = false)
    {
        if (!$strict) {
            return !!preg_match('/(^\d{15}$)|(^\d{17}([0-9]|X|x)$)/', $idCard);
        }
        if (strlen($idCard) == 18) {
            return self::idCardChecksum18($idCard);
        } elseif (strlen($idCard) == 15) {
            return self::idCardChecksum18(self::idCard15to18($idCard));
        }
        return false;
    }
    /**
     * 计算身份证校验码，根据国家标准GB 11643-1999
     * @param $idCardBase
     * @return bool|mixed
     */
    private static function idCardVerifyNumber($idCardBase)
    {
        if (strlen($idCardBase) != 17) {
            return false;
        }
        $factor    = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        $verifyNum = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
        $checksum  = 0;
        for ($i = 0; $i < strlen($idCardBase); $i++) {
            $checksum += substr($idCardBase, $i, 1) * $factor[$i];
        }
        $mod = $checksum % 11;
        return $verifyNum[$mod];

    }

    /**
     * 将15位身份证升级到18位
     * @param $idCard
     * @return bool|string
     */
    private static function idCard15to18($idCard)
    {
        if (strlen($idCard) != 15) {
            return false;
        } else {
            if (array_search(substr($idCard, 12, 3), ['996', '997', '998', '999']) !== false) {
                $idCard = substr($idCard, 0, 6) . '18' . substr($idCard, 6, 9);
            } else {
                $idCard = substr($idCard, 0, 6) . '19' . substr($idCard, 6, 9);
            }
        }
        $idCard = $idCard . self::idCardVerifyNumber($idCard);
        return $idCard;
    }
    /**
     * 18位身份证校验码有效性检查
     * @param $idCard
     * @return bool
     */
    private static function idCardChecksum18($idCard)
    {
        if (strlen($idCard) != 18) {
            return false;
        }
        $idCardBase = substr($idCard, 0, 17);
        return self::idCardVerifyNumber($idCardBase) == strtoupper(substr($idCard, 17, 1));
    }

    /**
     * 根据身份证获取性别
     * @param string $idCard
     * @return string
     */
    public static function getSex(string $idCard)
    {
        if(empty($idCard) || !self::isIdCard($idCard)){
            return '未知';
        }
        if(strlen($idCard) == 15){
            $sexint = (int) substr($idCard,14,1);
        } else {
            $sexint = (int) substr($idCard, 16, 1);
        }
        return $sexint % 2 === 0 ? '女' : '男';
    }

    /**
     * 根据身份证获取出生日期
     * @param string $idCard
     * @return false|string
     */
    public static function getBorn(string $idCard)
    {
        if(empty($idCard) || !self::isIdCard($idCard)) return null;
        if(strlen($idCard) == 15){
            $birthday = '19'.substr($idCard,6,6);
        } else {
            $birthday = substr($idCard,6,8);
        }
        $birthday = preg_replace('/(.{4})(.{2})/',"$1-$2-",$birthday);
        return $birthday;
    }

    /**
     * 根据身份证年龄
     * @param string $idCard
     * @param int $addAge
     * @return false|float|int
     */
    public static function getAge(string $idCard)
    {
        if(empty($idCard) || !self::isIdCard($idCard)){
            return 0;
        }
        $born = self::getBorn($idCard);
        if(empty($born)) return -1;
        $now_time = strtotime("now");
        $born_time = strtotime($born);
        $age = floor(($now_time - $born_time)/86400/365);
        return $age<0 ? 0 : $age;
    }
    
}
<?php

namespace dkou\utils;
/**
 * 日期时间相关函数集
 * Class DateTimeUtils
 * @package dkou\utils
 */
class DateTimeUtils
{
    /**
     * 获取当前 Unix 时间戳和微秒数(用秒的小数表示)浮点数表示
     * 常用来计算代码段执行时间
     * @return float
     */
    function microtimeFloat()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * 获取毫秒级当前时间戳
     * @return false|float
     */
    function getMillisecond()
    {
        $time = explode (" ", microtime () );
        $time = $time [1] . ($time [0] * 1000);
        $time2 = explode ( ".", $time );
        $time = $time2 [0];
        return $time;
    }

    /**
     * 将一个Unix时间戳转换成“xx前”模糊时间表达方式
     * @param  mixed $timestamp Unix时间戳
     * @return string
     */
    public static function timeAgo($timestamp)
    {
        $etime = time() - $timestamp;
        if ($etime < 1) return '刚刚';
        $interval = array (
            12 * 30 * 24 * 60 * 60  =>  '年前 ('.date('Y-m-d', $timestamp).')',
            30 * 24 * 60 * 60       =>  '个月前 ('.date('m-d', $timestamp).')',
            7 * 24 * 60 * 60        =>  '周前 ('.date('m-d', $timestamp).')',
            24 * 60 * 60            =>  '天前',
            60 * 60                 =>  '小时前',
            60                      =>  '分钟前',
            1                       =>  '秒前'
        );
        foreach ($interval as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . $str;
            }
        }
        return date('Y-m-d H:i:s', $timestamp);
    }

    /**
     * 秒时间转为中文
     * @param $seconds
     * @param string $default
     * @return string
     */
    public static function convertToCn($seconds, string $default = '')
    {
        $str     = $default;
        $seconds = str_replace(',', '', $seconds);
        if (!is_numeric($seconds)) {
            return $str;
        }
        $splits = [
            'year'   => [
                'explain' => '年',
                'val'     => 3600 * 24 * 365
            ],
            'day'    => [
                'explain' => '天',
                'val'     => 3600 * 24
            ],
            'hour'   => [
                'explain' => '小时',
                'val'     => 3600
            ],
            'minute' => [
                'explain' => '分',
                'val'     => 60
            ],
            'second' => [
                'explain' => '秒',
                'val'     => 1
            ],
        ];
        $str = '';
        foreach ($splits as $key => $item) {
            if ($seconds >= $item['val']) {
                $str    .= floor($seconds / $item['val']) . $item['explain'];
                $seconds = ($seconds % $item['val']);
            }
        }
        return $str;
    }

    /**
     * 计算两个时间差
     * @param $beginTime
     * @param $endTime
     * @return array
     */
    public static function timeDiff($beginTime, $endTime)
    {
        if($beginTime < $endTime){
            $startAt = $beginTime;
            $endAt = $endTime;
        }else{
            $startAt = $endTime;
            $endAt = $beginTime;
        }
        //计算天数
        $timeDiff = $endAt - $startAt;
        $days = intval($timeDiff/86400);
        //计算小时数
        $remain = $timeDiff % 86400;
        $hours = intval($remain/3600);
        //计算分钟数
        $remain = $remain % 3600;
        $mins = intval($remain/60);
        //计算秒数
        $secs = empty( $remain % 60 ) ? 0 : $remain % 60;
        $res = ["day" => $days,"hour" => $hours,"min" => $mins,"sec" => $secs];
        return $res;
    }
    
}
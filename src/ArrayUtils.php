<?php

namespace dkou\utils;
/**
 * 数组相关函数集
 * Class ArrayUtils
 * @package dkou\utils
 */
class ArrayUtils
{
    /**
     * 根据权重随机抽取一个元素
     * @param $arr
     */
    public static function getRandByWeight($arr)
    {
        $weight = 0;
        $tempData = array ();
        foreach ($arr as $one) {
            $weight += $one['weight'];
            for ($i = 0; $i < $one['weight']; $i++) {
                $tempData[] = $one;
            }
        }
        $use = rand(0, $weight -1);
        $one = $tempData[$use];
        return $one;
    }
}
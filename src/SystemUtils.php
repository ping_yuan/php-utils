<?php

namespace dkou\utils;
/**
 * 系统相关函数集
 * Class SystemUtils
 * @package dkou\utils
 */
class SystemUtils
{
    /**
     * 获取内存使用情况
     * @return int|string
     */
    public static function getMemoryUsage()
    {
        if(function_exists('memory_get_usage')){
            $mem = memory_get_usage();
            return FileUtils::byteFormat($mem);
        }
        return 0;
    }

}
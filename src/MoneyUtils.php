<?php

namespace dkou\utils;
/**
 * 金额相关函数集
 * Class MoneyUtils
 * @package dkou\utils
 */
class MoneyUtils
{
    /**
     * 分转为元
     * @param string|int $price 金额，单位：分
     * @return string
     */
    public static function fenToYuan($price)
    {
        return self::calc(self::format($price), "/", 100);
    }

    /**
     * 元转为分
     * @param string|int $price 金额，单位：元
     * @return int
     */
    public static function yuanToFen($price)
    {
        return intval(self::calc(100, "*", $price));
    }

    /**
     * 高精度计算
     * @param $n1           string 第一个数
     * @param $symbol       string 计算符号 + - * / %
     * @param $n2           string 第一个数
     * @param string $scale string
     * @return string
     */
    public static function calc($n1, $symbol, $n2, $scale = '2')
    {
        switch ($symbol) {
            case "+"://加法
                $res = bcadd($n1, $n2, $scale);
                break;
            case "-"://减法
                $res = bcsub($n1, $n2, $scale);
                break;
            case "*"://乘法
                $res = bcmul($n1, $n2, $scale);
                break;
            case "/"://除法
                $res = bcdiv($n1, $n2, $scale);
                break;
            case "%"://求余、取模
                $res = bcmod($n1, $n2, $scale);
                break;
            default:
                $res = "";
                break;
        }
        return $res;
    }

    /**
     * 金额格式化，统一精确到分，如：100->100.00
     * @param  string|int $price 金额,单位：元
     * @return string
     */
    public static function format($price)
    {
       return number_format($price, 2, '.', '');
    }

    /**
     * 大金额加上万、亿单位
     * @param $amount string|int 金额,单位：元
     * @return string
     */
    public static function convertBigAmount($amount)
    {
        $length = strlen($amount);  //数字长度
        if($length > 8){
            //亿单位
            $str = substr_replace(strstr($amount,substr($amount,-7),' '),'.',-1,0)."亿";
        }elseif($length >4){
            //万单位,截取前两位
            $str = substr_replace(strstr($amount,substr($amount,-3),' '),'.',-1,0)."万";
        }else{
            return $amount;
        }
        return $str;
    }

    /**
     * 将数值金额转换为中文大写金额
     * @param $amount float 金额(支持到分)
     * @param $type   int   补整类型,0:到角补整;1:到元补整
     * @return mixed
     */
    public static function convertToCn($amount, $type = 1) {
        // 判断输出的金额是否为数字或数字字符串
        if(!is_numeric($amount)){
            return "要转换的金额只能为数字!";
        }
        // 金额为0,则直接输出"零元整"
        if($amount == 0) {
            return "人民币零元整";
        }
        // 金额不能为负数
        if($amount < 0) {
            return "要转换的金额不能为负数!";
        }
        // 金额不能超过万亿,即12位
        if(strlen($amount) > 12) {
            return "要转换的金额不能为万亿及更高金额!";
        }
        // 预定义中文转换的数组
        $digital = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        // 预定义单位转换的数组
        $position = ['仟', '佰', '拾', '亿', '仟', '佰', '拾', '万', '仟', '佰', '拾', '元'];
        // 将金额的数值字符串拆分成数组
        $amountArr = explode('.', $amount);
        // 将整数位的数值字符串拆分成数组
        $integerArr = str_split($amountArr[0], 1);
        // 将整数部分替换成大写汉字
        $result = '人民币';
        $integerArrLength = count($integerArr);     // 整数位数组的长度
        $positionLength = count($position);         // 单位数组的长度
        $zeroCount = 0;                             // 连续为0数量
        for($i = 0; $i < $integerArrLength; $i++) {
            // 如果数值不为0,则正常转换
            if($integerArr[$i] != 0){
                // 如果前面数字为0需要增加一个零
                if($zeroCount >= 1){
                    $result .= $digital[0];
                }
                $result .= $digital[$integerArr[$i]] . $position[$positionLength - $integerArrLength + $i];
                $zeroCount = 0;
            }else{
                $zeroCount += 1;
                // 如果数值为0, 且单位是亿,万,元这三个的时候,则直接显示单位
                if(($positionLength - $integerArrLength + $i + 1)%4 == 0){
                    $result = $result . $position[$positionLength - $integerArrLength + $i];
                }
            }
        }
        // 如果小数位也要转换
        if($type == 0) {
            // 将小数位的数值字符串拆分成数组
            $decimalArr = str_split($amountArr[1], 1);
            // 将角替换成大写汉字. 如果为0,则不替换
            if($decimalArr[0] != 0){
                $result = $result . $digital[$decimalArr[0]] . '角';
            }
            // 将分替换成大写汉字. 如果为0,则不替换
            if($decimalArr[1] != 0){
                $result = $result . $digital[$decimalArr[1]] . '分';
            }
        }else{
            $result = $result . '整';
        }
        return $result;
    }
}               
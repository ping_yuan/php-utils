# PHP工具类合集
常用函数收集、归类，统一调用方式，拿来即用。

## 背景
PHP发展至今，在网上其实有一系列经过前人整理出来非常优秀的函数，而大多数的PHPer更倾向于拿来即用，目前暂时没有一个类似Java的HuTool一样的工具集，把网上可复用的，与业务无关的工具函数整理起来。减少重复定义，提高效率。

## 优点
- 对网上优秀的工具函数收集并分类整理，目前已有分类：Str、Array、DateTime、File、Vaildate、Money、Curl、Img等。
- 统一调用方式，减轻运行压力，调用起来非常优雅。
- 持续更新。

## 工具类列表
|  工具类名   | 说明  |
|  :----  | :----  |
| ArrayUtils  | 数组工具类 |
| CurlUtils  | curl工具类 |
| DateTimeUtils  | 日期时间工具类 |
| DesensitizeUtils  | 数据脱敏相工具类 |
| FileUtils  | 文件工具类 |
| FormatUtils  | 数据格式化工具类 |
| IdCardUtils  | 身份证号码工具类 |
| ImgUtils  | 图片工具类 |
| MoneyUtils  | 金额工具类 |
| RequestUtils  | 请求工具类 |
| ServerUtils  | 请求服务端工具类 |
| StrUtils  | 字符串工具类 |
| SystemUtils  | 系统信息工具类 |
| TreeUtils  | 二叉树工具类 |
| ValidateUtils  | 数据校验工具类 |



